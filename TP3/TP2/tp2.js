window.addEventListener("load", ready);		// Comme toujours, on attend que la page soit chargée
											// avant de faire quoi que ce soit.

function ready(){
	// Au chargement de la page il n'y a que 2 événements à gérer, vu qu'il n'existe
	// qu'une seule alarme ainsi que le bouton d'ajout (dans notre cas).
	
	// Supression et activation pour la première alarme
	addEvents(document.getElementById("alarm"));
	
	// Evenement d'ajout
	document.getElementById("add_alarm")
		.addEventListener("click", function() {
			// Pour ajouter un champ d'alarme, on clone la première alarme existante
			// (qui est invisible pour ne pas qu'elle soit supprimée) puis on l'ajoute au DOM
			// pour la placer sur la page.
			var alarmNode = document.getElementsByClassName("alarm")[0].cloneNode(true);
			document.getElementById("alarms").appendChild(alarmNode);
			
			// ATTENTION : cloneNode ne clone pas les événements. 
			// Il faut donc les ajouter à chaque champ d'alarme créé !
			addEvents(alarmNode);
		})
	;
	
	// Synchronisation de l'horloge et des alarmes
	setInterval(function(){
		// Mise à jour d'horloge
		var date = new Date();
		document.getElementById("time").textContent = date.toLocaleTimeString(); 
		
		// Vérification des alarmes
		var alarms = document.getElementsByClassName("alarm");
		for (var alarm of alarms) {
			if (alarm.getElementsByClassName("alarm_check")[0].checked) {
				if ((date.getHours() === parseInt(alarm.getElementsByClassName("alarm_hour")[0].value)) &&
					(date.getMinutes() === parseInt(alarm.getElementsByClassName("alarm_min")[0].value)) &&
					(date.getSeconds() === 0)) {
					//runAudio(alarm.getElementsByClassName("alarm_sound")[0].value);
					var player = document.querySelector('#audio');
					player.play();
					alert(alarm.getElementsByClassName("alarm_msg")[0].value);
					// Mettre une pause ici indique que la musique ne va s'arrêter
					// qu'une fois le pop-up d'alerte fermé.
					document.getElementById("audio").pause();
				}
			}
		}
	}, 500);	// Toute cette fonction anonyme va donc s'exécuter 2 fois par seconde. 
				// N'importe quelle valeur en dessous d'1 seconde paraît acceptable ici.
}

function addEvents(node) {
	// Evenements de suppression
	node.getElementsByClassName("del_alarm")[0]
		.addEventListener("click", function() {
			document.getElementById("alarms").removeChild(node);
		})
		// On peut aussi récupérer la cible de l'événement en utilisant 
		// l'événément passé en paramètre et son attribute 'target'.
		// On aurait alors:
		/*
		.addEventListener("click", function(e) {
			document.getElementById("alarms").removeChild(e.target.parentNode);
		})
		*/
	;

	// Evenement d'activation (le changement de couleur)
	var cbx = node.getElementsByClassName("alarm_check")[0];
	cbx.addEventListener("change", function() {
			if (cbx.checked) {
				cbx.parentNode.classList.remove("unchecked");
				cbx.parentNode.className += " checked";
			} else {
				cbx.parentNode.classList.remove("checked");
				cbx.parentNode.className += " unchecked";
			}
		})
	;
}


function runAudio(song) {
	var audio = document.getElementById("audio");
	audio.src = song + '.mp3';
	audio.play();
}


