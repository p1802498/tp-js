	function disabledbled(){
		document.getElementsByClassName("btn")[0].disabled = true;		// on Load du doc on diabled true le bouton submit

	}
    function validatePassword(password) {
        
        if (password.length === 0) {
            document.getElementById("id_password1").innerHTML = "";
            return;
        }
        
        var matchedCase = new Array();
        matchedCase.push("[$@$!%*#?&]"); // Special Charector
        matchedCase.push("[A-Z]");      // Uppercase Alpabates
        matchedCase.push("[0-9]");      // Numbers
        matchedCase.push("[a-z]");     // Lowercase Alphabates
  
        // Check the conditions
        var ctr = 0;
        for (var i = 0; i < matchedCase.length; i++) {
            if (new RegExp(matchedCase[i]).test(password)) {
                ctr++;
            }
        }
        // Display it
        var color = "";
		var strength = "";
		var progdiv1 = document.getElementById("progdiv");
		console.log(progdiv1);
		console.log(progdiv1.getElementsByClassName("progress-bar")[0].style);
        switch (ctr) {
            case 0:
            case 1:
            case 2:
				strength = "Very Weak";
				progdiv1.getElementsByClassName("progress-bar")[0].style.width = 25 + "%"; //progression de la barre
                color = "red";
                break;
            case 3:
				strength = "Medium";
				progdiv1.getElementsByClassName("progress-bar")[0].style.width = 75 + "%"; //progression de la barre

                color = "orange";
                break;
            case 4:
				if (document.getElementById("id_password1").value.length >= 8){
				strength = "Strong";
				progdiv1.getElementsByClassName("progress-bar")[0].style.width = 100 + "%"; //progression de la barre
			}
			color = "green";

                break;
        }
		document.getElementById("id_password1").innerHTML = strength;  // on affiche  la force (mais au final je l'utilise pas)
		document.getElementById("id_password1").style.color = color;
		return strength;
	}
	
	function samePassword(){
		console.log(document.getElementById("id_password1").value);
		console.log(document.getElementById("id_password2").value);
		if(document.getElementById("id_password1").value == document.getElementById("id_password1").value){ 	//si les mdp sont les meme
			return true;																						// la fonction return true
		}
		else {
			return false;
		}
	}
  
	function checkAge(){
		console.log(document.getElementsByClassName("tentacles")[0].value);
		if(document.getElementsByClassName("tentacles")[0].value >= 18){		//si l'age est > a 18
			return true;														//la fonction return true
		}else {
			return false;
		}
	}
	function checkIdt(){
		var regexID = new RegExp("^[a-zA-Z]*$");
		if(document.getElementsByClassName("userName1")[0].value.length <= 12 && regexID.test(document.getElementsByClassName("userName1")[0].value)){ //si l'userName n'a que des lettres
			return true;																															//la fonction return true
		}else{
			return false;
		}
	}
	function checkCGU(){	
		if(document.getElementsByClassName("checkboxinput")[0].checked){ // si la checkBox est checked 
			return true;												//la fonction return true
		}else{
			return false;
		}
	}

	function validateAll(){
		console.log("age " + checkAge());	//debug pour check les fonctions du dessus
		console.log("CGU " + checkCGU());
		console.log("Idt " + checkIdt());
		console.log(samePassword());
		console.log(validatePassword(document.getElementById("id_password1").value));
		console.log(document.getElementsByClassName("btn")[0]);
		if(checkAge() && checkCGU() && checkIdt() && samePassword() && validatePassword(document.getElementById("id_password1").value) == "Strong"){ // si elles retournent toute true
			document.getElementsByClassName("btn")[0].disabled = false;				// on disabled false le bouton submit
		}
	}
